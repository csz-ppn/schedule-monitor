Parses and detects changes in iCal calendars, as served by TimeEdit at LTH. (The script is rather crude and most likely doesn't work for any iCal data except TimeEdit at LTH -- if at all.)

The script is suited to run as a cron job. Every time it is run, the
contents of the given subscription is compared to its previous state, and any
changes are sent the configured e-mail address.

# cron configuration

Ensure the x flag is set (chmod u+x schedmon.py).

To run the script once every hour, I use a crontab line like this:

    0      *       *       *       *       /<path>/schedmon.py <ical url> <email address>

You will obviously need to adjust the path according to your own configuration. Use tab characters between columns.