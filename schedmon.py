#!/usr/bin/env python

# -----------------------------------------------------------------------------
# Parses and detects changes in iCal calendars.
#
# The script is suited to run as a cron job. Every time the script is run, the
# contents of the given subscription is compared to its previous state, and any
# changes are sent the configured e-mail address.
#
# WARNING: hard-coded to work with LTH TimeEdit calendars, assuming:
#  - iCal data is well-formed; no error checks are made
#  - timestamps are in UTC
#  - strings within events are given in the same order each time
#  - script to run within Lund University network (accessing mail.lu.se)
#  - ... and, most likely, much more.
#
# -----------------------------------------------------------------------------
# USAGE:
#   schedmon.py ICAL-URL EMAIL-ADDRESS
# -----------------------------------------------------------------------------
#
# Copyright (c) 2015 Patrik Persson
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
# -----------------------------------------------------------------------------

import calendar
import datetime
import urllib2
import os.path
import smtplib
import sys

# -----------------------------------------------------------------------------

def to_localtime(utc_ical_date):
    """Convert iCal UTC date string to datetime object in local time"""
    utc_dt = datetime.datetime.strptime(utc_ical_date, "%Y%m%dT%H%M%SZ")
    timestamp = calendar.timegm(utc_dt.timetuple())
    local_dt = datetime.datetime.fromtimestamp(timestamp)
    return local_dt.replace(microsecond=utc_dt.microsecond)

# -----------------------------------------------------------------------------

class Event(object):
    
    def __init__(self):
        self.strings = []
    
    def append(self, line):
        if line.startswith(' '):
            self.strings[-1] += line[1:]
        else:
            self.strings += [line]

    def is_future(self):
        """returns true if this event refers to the future (i.e., after today)"""
        today = datetime.date.today()
        today_str = "%04d%02d%02d" % (today.year, today.month, today.day)
        for s in self.strings:
            if s.startswith("DTSTART"):
                date = s.split(':', 1)[1][:8]
                return (date > today_str)
        raise Exception("event missing DTSTART tag")

    def __eq__(self, other):
        return str(self) == str(other)
    
    def __ne__(self, other):
        return str(self) != str(other)

    def __str__(self):
        dtstart = ""
        dtend = ""
        location = ""
        summary = ""
        descr = ""
        
        for s in self.strings:
            tag, value = s.split(':', 1)
            value = value.replace("\\n", ";").replace("\\", "")
            if tag.startswith("DTSTART"):
                dtstart = value
            elif tag.startswith("DTEND"):
                dtend = value
            elif tag.startswith("LOCATION") and value != "":
                location = "(" + value + ") "
            elif tag.startswith("SUMMARY"):
                summary = value
            elif tag.startswith("DESCRIPTION") and value != "":
                descr = " [" + value + "]"

        # create human-readable representation of date as 'dt'
        if len(dtstart) == 8: # date, no time
            if dtstart == dtend:
                dt = "%s-%s-%s" % (dtstart[:4], dtstart[4:6], dtstart[6:])
            else:
                dt = "%s-%s-%s - %s-%s-%s" % (dtstart[:4], dtstart[4:6], dtstart[6:], dtend[:4], dtend[4:6], dtend[6:])
        else:
            # assume single-day event
            dt = to_localtime(dtstart).strftime("%Y-%m-%d %H:%M") + to_localtime(dtend).strftime("-%H:%M")

        return dt + " " + location + summary + descr

# -----------------------------------------------------------------------------

class Calendar(object):
    
    def __init__(self, lines):
        self.entries = {}  # uid->Event
        
        curr_event = None
        for line in lines:
            line = line.rstrip('\r\n')
            if line.startswith('BEGIN:VEVENT'):
                curr_event = Event()
            elif line.startswith('END:VEVENT'):
                if curr_event.is_future():
                    self.entries[uid] = curr_event
                curr_event = None
            elif line.startswith('UID:'):
                (_, uid) = line.split(':',1)
            elif curr_event and not (line.startswith('DTSTAMP:') or line.startswith('LAST-MODIFIED:')):
                curr_event.append(line)

    def diff(self, newer):
        """Returns array of lines prefixed with '+' or '-'."""
        lines = []
        for uid in newer.entries:
            new_entry = newer.entries.get(uid)
            old_entry = self.entries.get(uid)
            if not old_entry:
                lines += ["+ %s" % new_entry]
            elif old_entry != new_entry:
                lines += ["- %s" % old_entry]
                lines += ["+ %s" % new_entry]

        for uid in self.entries:
            new_entry = newer.entries.get(uid)
            old_entry = self.entries.get(uid)
            if not new_entry:
                lines += ["- %s" % old_entry]

        return lines

# -----------------------------------------------------------------------------

SMTP_SERVER='mail.lu.se'
PREV_FILE='.schedmon.prev'

url=sys.argv[1]
email=sys.argv[2]

resp = urllib2.urlopen(url)
code = resp.code
type = resp.headers['content-type']
if not (code == 200 and type.startswith('text/calendar')):
    print "HTTP access to calendar failed: error %d, type %s" % (code, type)
    sys.exit(resp.code)

curr_data = resp.read()
curr_cal = Calendar(curr_data.split('\n'))

if os.path.isfile(PREV_FILE):
    with open(PREV_FILE) as f:
        prev_cal = Calendar(f.readlines())
        lines = prev_cal.diff(curr_cal)
        if lines != []:
            message = "To: %s\nSubject: Schedule changed\n\n%s" % (email, '\n'.join(lines))
            server = smtplib.SMTP(SMTP_SERVER)
            server.sendmail(email, email, message)
            server.quit()
        f.close()

with open(PREV_FILE, 'w') as f:
    f.write(curr_data)
    f.close()
